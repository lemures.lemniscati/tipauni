-------------------------------------------------------------
-- Build file: build.lua
-- Consisted in the LaTeX package `tipauni'
-- Copyright © 2021, 2022 निरंजन
--
-- This program is free software: you can redistribute it
-- and/or modify it under the terms of the GNU General
-- Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your
-- option) any later version.
--
-- This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the
-- implied warranty of MERCHANTABILITY or FITNESS FOR A
-- PARTICULAR PURPOSE. See the GNU General Public License
-- for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program. If not, see
-- <https://www.gnu.org/licenses/>.
-------------------------------------------------------------
module     = "tipauni"
pkgversion = "0.5"
pkgdate    = os.date("%Y-%m-%d")

-- Tagging:
tagfiles = {"tipauni.dtx", "README.txt", "tipauni.ins"}
function update_tag(file, content, tagname, tagdate)
   if tagname == nil then
      tagname = pkgversion
      tagdate = pkgdate
   end
   if string.match(content,"Version:      v%d+%.%d+%w? %(%d+ %B, %d+%)\n") then
      content = string.gsub(content,"Version:      v%d+%.%d+%w? %(%d+ %B, %d+%)\n",
            "Version:      v" .. pkgversion .. " (" .. os.date("%d %B, %Y") .. ")\n")
   end
   if string.match(content,"\\def\\tipauniversion{%d+%.%d+%w?}\n") then
      content = string.gsub(content,"\\def\\tipauniversion{%d+%.%d+%w?}\n",
            "\\def\\tipauniversion{" .. pkgversion .. "}\n")
   end
   if string.match(content,"\\def\\tipaunidate{%d+-%d+-%d+}\n") then
      content = string.gsub(content,"\\def\\tipaunidate{%d+-%d+-%d+}\n",
            "\\def\\tipaunidate{" .. pkgdate .. "}\n")
   end
   if string.match(content,"LaTeX Package tipauni v%d+%.%d+%w?\n") then
      content = string.gsub(content,"LaTeX Package tipauni v%d+%.%d+%w?\n",
            "LaTeX Package tipauni v" .. pkgversion .. "\n")
   end
   return content
end

-- Checking:
checkengines = { "luatex", "xetex" }
stdengine    = "luatex"
checkruns    = 3

-- Documentation:
typesetexe   = "lualatex"
typesetruns  = 3
typesetsuppfiles = { "gfdl-tex.tex" }
typesetfiles = { "tipauni.dtx", "tipauni-example.tex", "tipauni-commands.tex" }
docfiles     = { "COPYING" }

-- CTAN upload
ctanreadme    = "README.txt"
uploadconfig  = {
   pkg         = module,
   author      = "निरंजन",
   email       = "hi.niranjan@pm.me",
   uploader    = "निरंजन",
   version     = pkgversion .. " " .. pkgdate,
   license     = "GPLv3+, GFDLv1.3+",
   summary     = "Producing Unicode characters with TIPA commands",
   topic       = "Phonetic, Unicode, Linguistic",
   ctanPath    = "/macros/unicodetex/latex/tipauni",
   repository  = "https://git.gnu.org.ua/tipauni.git",
   bugtracker  = "https://puszcza.gnu.org.ua/bugs/?group=tipauni",
   update      = true,
   description = [[Package TIPA uses the T3 encoding for
producing IPA characters. The package is widely used in the
field of linguistics, but because of the old encoding, the
output documents are less productive than Unicode-based
documents.

This package redefines most of the TIPA-commands for
outputting Unicode characters. Users can now use their
beloved TIPA shortcuts with the benefits of Unicode,
i.e. searchability, copy-pasting, changing the font and many
more.

As this package needs the fontspec package for loading an
IPA font, it needs to be compiled with XeLaTeX or LuaLaTeX.

This package can also be viewed as an ASCII-based input
method for producing IPA characters in Unicode.

It needs the Charis SIL font for printing IPA characters.]]
}
