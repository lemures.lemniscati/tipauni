--------------------------------------------------------------------------
Package:      tipauni
Author:       निरंजन
Version:      v0.4.1 (03 January, 2022)
Description:  For producing Unicode characters with TIPA commands.
Repository:   https://git.gnu.org.ua/tipauni.git
Bug tracker:  https://puszcza.gnu.org.ua/bugs/?group=tipauni
License:      GPL v3.0+, GFDL v1.3+
--------------------------------------------------------------------------
